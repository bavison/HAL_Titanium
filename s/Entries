;
; CDDL HEADER START
;
; The contents of this file are subject to the terms of the
; Common Development and Distribution License (the "Licence").
; You may not use this file except in compliance with the Licence.
;
; You can obtain a copy of the licence at
; cddl/RiscOS/Sources/HAL/Titanium/LICENCE.
; See the Licence for the specific language governing permissions
; and limitations under the Licence.
;
; When distributing Covered Code, include this CDDL HEADER in each
; file and include the Licence file. If applicable, add the
; following below this CDDL HEADER, with the fields enclosed by
; brackets "[]" replaced with your own identifying information:
; Portions Copyright [yyyy] [name of copyright owner]
;
; CDDL HEADER END
;
;
; Copyright 2014 Elesar Ltd.  All rights reserved.
; Use is subject to license terms.
;

        GET     Hdr:ListOpts
        GET     Hdr:Macros
        GET     Hdr:System
        GET     Hdr:ImageSize.<ImageSize>
        GET     Hdr:HALSize.<HALSize>
        GET     Hdr:OSEntries
        GET     Hdr:HALEntries

        GET     hdr.RegMap
        GET     hdr.StaticWS

        AREA    |Entries$$Code|, CODE, READONLY, PIC

        IMPORT  GPHeader
        EXPORT  EntryDescriptor
        IMPORT  HAL_Init
        IMPORT  HAL_InitDevices
        IMPORT  HAL_DebugTX
        IMPORT  HAL_DebugRX
        IMPORT  HAL_IRQMax
        IMPORT  HAL_IICBuses
        IMPORT  HAL_IICDevice
        IMPORT  HAL_IICMonitorTransfer
        IMPORT  HAL_IICTransfer
        IMPORT  HAL_IICType
        IMPORT  HAL_Timers
        IMPORT  HAL_TimerDevice
        IMPORT  HAL_TimerGranularity
        IMPORT  HAL_TimerMaxPeriod
        IMPORT  HAL_TimerSetPeriod
        IMPORT  HAL_TimerPeriod
        IMPORT  HAL_TimerReadCountdown
        IMPORT  HAL_TimerIRQClear
        IMPORT  HAL_TimerIRQStatus
        IMPORT  HAL_IRQEnable
        IMPORT  HAL_IRQDisable
        IMPORT  HAL_IRQClear
        IMPORT  HAL_IRQSource
        IMPORT  HAL_IRQStatus
        IMPORT  HAL_NVMemoryType
        IMPORT  HAL_NVMemorySize
        IMPORT  HAL_NVMemoryPageSize
        IMPORT  HAL_NVMemoryIICAddress
        IMPORT  HAL_KbdScanDependencies
        IMPORT  HAL_USBControllerInfo
        IMPORT  HAL_USBPortPower
        IMPORT  HAL_USBPortIRQStatus
        IMPORT  HAL_USBPortIRQClear
        IMPORT  HAL_USBPortDevice
        IMPORT  HAL_CounterRate
        IMPORT  HAL_CounterPeriod
        IMPORT  HAL_CounterRead
        IMPORT  HAL_CounterDelay
        IMPORT  HAL_Reset
        IMPORT  HAL_PhysInfo
        IMPORT  HAL_MachineID
        IMPORT  HAL_HardwareInfo
        IMPORT  HAL_SuperIOInfo
        IMPORT  HAL_ControllerAddress
        IMPORT  HAL_PlatformInfo
        IMPORT  HAL_PlatformName
        IMPORT  HAL_PCIReadConfigByte
        IMPORT  HAL_PCIReadConfigHalfword
        IMPORT  HAL_PCIReadConfigWord
        IMPORT  HAL_PCIWriteConfigByte
        IMPORT  HAL_PCIWriteConfigHalfword
        IMPORT  HAL_PCIWriteConfigWord
        IMPORT  HAL_PCISlotTable
        IMPORT  HAL_PCIAddresses
        IMPORT  HAL_UARTDefault
        IMPORT  HAL_UARTPorts
        IMPORT  HAL_UARTStartUp
        IMPORT  HAL_UARTShutdown
        IMPORT  HAL_UARTFeatures
        IMPORT  HAL_UARTReceiveByte
        IMPORT  HAL_UARTTransmitByte
        IMPORT  HAL_UARTLineStatus
        IMPORT  HAL_UARTInterruptEnable
        IMPORT  HAL_UARTRate
        IMPORT  HAL_UARTFormat
        IMPORT  HAL_UARTFIFOSize
        IMPORT  HAL_UARTFIFOClear
        IMPORT  HAL_UARTFIFOEnable
        IMPORT  HAL_UARTFIFOThreshold
        IMPORT  HAL_UARTInterruptID
        IMPORT  HAL_UARTBreak
        IMPORT  HAL_UARTModemControl
        IMPORT  HAL_UARTModemStatus
        IMPORT  HAL_UARTDevice

EntryDescriptor
        DCD     0
        DCD     GPHeader - EntryDescriptor
        DCD     OSROM_HALSize
        DCD     EntryTable - EntryDescriptor
        DCD     EntryTableSize
        DCD     :INDEX:StaticWSSize

        MACRO
        HALEntry $name
        ASSERT  (. - EntryTable) / 4 = EntryNo_$name
        DCD     $name - EntryTable
        MEND

        MACRO
        NullEntry
        DCD     HAL_Null - EntryTable
        MEND

EntryTable
        HALEntry HAL_Init

        HALEntry HAL_IRQEnable
        HALEntry HAL_IRQDisable
        HALEntry HAL_IRQClear
        HALEntry HAL_IRQSource
        HALEntry HAL_IRQStatus
        NullEntry ;HAL_FIQEnable
        NullEntry ;HAL_FIQDisable
        NullEntry ;HAL_FIQDisableAll
        NullEntry ;HAL_FIQClear
        NullEntry ;HAL_FIQSource
        NullEntry ;HAL_FIQStatus

        HALEntry HAL_Timers
        HALEntry HAL_TimerDevice
        HALEntry HAL_TimerGranularity
        HALEntry HAL_TimerMaxPeriod
        HALEntry HAL_TimerSetPeriod
        HALEntry HAL_TimerPeriod
        HALEntry HAL_TimerReadCountdown

        HALEntry HAL_CounterRate
        HALEntry HAL_CounterPeriod
        HALEntry HAL_CounterRead
        HALEntry HAL_CounterDelay

        HALEntry HAL_NVMemoryType
        HALEntry HAL_NVMemorySize
        HALEntry HAL_NVMemoryPageSize
        NullEntry ;HAL_NVMemoryProtectedSize
        NullEntry ;HAL_NVMemoryProtection
        HALEntry HAL_NVMemoryIICAddress
        NullEntry ;HAL_NVMemoryRead
        NullEntry ;HAL_NVMemoryWrite

        HALEntry HAL_IICBuses
        HALEntry HAL_IICType
        NullEntry ;HAL_IICSetLines
        NullEntry ;HAL_IICReadLines
        HALEntry HAL_IICDevice
        HALEntry HAL_IICTransfer
        HALEntry HAL_IICMonitorTransfer

        NullEntry ;HAL_VideoFlybackDevice
        NullEntry ;HAL_VideoSetMode
        NullEntry ;HAL_VideoWritePaletteEntry
        NullEntry ;HAL_VideoWritePaletteEntries
        NullEntry ;HAL_VideoReadPaletteEntry
        NullEntry ;HAL_VideoSetInterlace
        NullEntry ;HAL_VideoSetBlank
        NullEntry ;HAL_VideoSetPowerSave
        NullEntry ;HAL_VideoUpdatePointer
        NullEntry ;HAL_VideoSetDAG
        NullEntry ;HAL_VideoVetMode
        NullEntry ;HAL_VideoPixelFormats
        NullEntry ;HAL_VideoFeatures
        NullEntry ;HAL_VideoBufferAlignment
        NullEntry ;HAL_VideoOutputFormat

        NullEntry ;HAL_MatrixColumns
        NullEntry ;HAL_MatrixScan

        NullEntry ;HAL_TouchscreenType
        NullEntry ;HAL_TouchscreenRead
        NullEntry ;HAL_TouchscreenMode
        NullEntry ;HAL_TouchscreenMeasure

        HALEntry HAL_MachineID
        HALEntry HAL_ControllerAddress
        HALEntry HAL_HardwareInfo
        HALEntry HAL_SuperIOInfo
        HALEntry HAL_PlatformInfo
        NullEntry ;HAL_CleanerSpace

        HALEntry HAL_UARTPorts
        HALEntry HAL_UARTStartUp
        HALEntry HAL_UARTShutdown
        HALEntry HAL_UARTFeatures
        HALEntry HAL_UARTReceiveByte
        HALEntry HAL_UARTTransmitByte
        HALEntry HAL_UARTLineStatus
        HALEntry HAL_UARTInterruptEnable
        HALEntry HAL_UARTRate
        HALEntry HAL_UARTFormat
        HALEntry HAL_UARTFIFOSize
        HALEntry HAL_UARTFIFOClear
        HALEntry HAL_UARTFIFOEnable
        HALEntry HAL_UARTFIFOThreshold
        HALEntry HAL_UARTInterruptID
        HALEntry HAL_UARTBreak
        HALEntry HAL_UARTModemControl
        HALEntry HAL_UARTModemStatus
        HALEntry HAL_UARTDevice
        HALEntry HAL_UARTDefault

        HALEntry HAL_DebugRX
        HALEntry HAL_DebugTX

        NullEntry ;HAL_PCIFeatures
        HALEntry HAL_PCIReadConfigByte
        HALEntry HAL_PCIReadConfigHalfword
        HALEntry HAL_PCIReadConfigWord
        HALEntry HAL_PCIWriteConfigByte
        HALEntry HAL_PCIWriteConfigHalfword
        HALEntry HAL_PCIWriteConfigWord
        NullEntry ;HAL_PCISpecialCycle
        HALEntry HAL_PCISlotTable
        HALEntry HAL_PCIAddresses

        HALEntry HAL_PlatformName
        NullEntry ;Unused
        NullEntry ;Unused

        HALEntry HAL_InitDevices

        HALEntry HAL_KbdScanDependencies
        NullEntry ;Unused
        NullEntry ;Unused
        NullEntry ;Unused

        HALEntry HAL_PhysInfo

        HALEntry HAL_Reset

        HALEntry HAL_IRQMax

        HALEntry HAL_USBControllerInfo
        HALEntry HAL_USBPortPower
        HALEntry HAL_USBPortIRQStatus
        HALEntry HAL_USBPortIRQClear
        HALEntry HAL_USBPortDevice

        HALEntry HAL_TimerIRQClear
        HALEntry HAL_TimerIRQStatus

EntryTableSize  * (. - EntryTable):SHR:2

; HAL_Null
HAL_Null ROUT
        BX      lr

        END
